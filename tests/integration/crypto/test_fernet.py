import cryptography.fernet

TEST_KEY = "kS0CSInD5Mr5N3yzn_RjI9GG5UCflHcZaZH6cFxe2a8="


def test_generate_key(hub):
    """
    Verify that a valid fernet key is always generated
    """
    for _ in range(100000):
        key = hub.crypto.fernet.generate_key()
        cryptography.fernet.Fernet(key)


def test_encrypt(hub):
    data = {"1": "sdaf", "2": ["1", "", [], {}]}

    encrypted = hub.crypto.fernet.encrypt(data, TEST_KEY)
    assert encrypted != data
    assert isinstance(encrypted, bytes)


def test_decrypt(hub):
    data = {"1": "sdaf", "2": ["1", "", [1, 2, 3], {}]}

    encrypted = hub.crypto.fernet.encrypt(data, TEST_KEY)
    decrypted = hub.crypto.fernet.decrypt(encrypted, TEST_KEY)

    assert decrypted == data
